package com.wsr.chatapplication.models

data class ModelChatAnswer <T>(
    val type: String,
    val body: T
)