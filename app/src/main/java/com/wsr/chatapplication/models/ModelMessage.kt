package com.wsr.chatapplication.models

data class ModelMessage(
    val message: String,
    val id: Int,
    val isYou: Boolean,
    val datetime: String?,
    val idUser: Int,
    val isAudio: Boolean
)

data class ModelItemMessage(
    val id: Int,
    val idChat: Int,
    val text: String,
    val datetime: String,
    val idUser: Int,
    val isAudio: Boolean
)