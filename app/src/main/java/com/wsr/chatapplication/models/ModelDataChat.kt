package com.wsr.chatapplication.models

data class ModelDataChat(
    val chat: ModelItemChat?,
    val message: List<ModelItemMessage>
)
