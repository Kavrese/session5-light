package com.wsr.chatapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.wsr.chatapplication.models.ModelDataChat
import com.wsr.chatapplication.models.ModelItemChat
import com.wsr.chatapplication.models.ModelMessage
import com.wsr.chatapplication.models.ModelUser

class ChatsActivity : AppCompatActivity(), Callback {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chats)

        Connection.callbacks.add(this)
        Connection.client.connect()

    }

    override fun onOpen() {
        Connection.client.send("/chats")
    }

    override fun onMessage(modelMessage: ModelMessage) {

    }

    override fun onChats(chats: List<ModelItemChat>) {
        runOnUiThread {
            Toast.makeText(this, "${chats.joinToString { it.toString() }} !", Toast.LENGTH_LONG).show()
        }
    }

    override fun onChat(chat: ModelDataChat) {

    }

    override fun onPerson(modelUser: ModelUser) {
        runOnUiThread {
            Toast.makeText(this, "Привет ${modelUser.firstname} !", Toast.LENGTH_LONG).show()
        }
    }


}